package BattleShip;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

import battleShip.Ship;

public class ShipTest {
	Ship ship;
	@BeforeEach
	public void Setup() {
		ship = new Ship(0,0,3,0);
	}
	@Test
	@DisplayName("Sjekker om getter for: x1,x2,y1 og y2 funker. Samt for lengden av skipet")
	public void testGetters() {
		ship = new Ship(0,0,3,0);
		assertEquals("X1",0,ship.getX1());
		assertEquals("Y1",0,ship.getY1());
		assertEquals("X2",3,ship.getX2());
		assertEquals("Y2",0,ship.getY2());
		assertEquals("Len",4,ship.getLen());
	}
	@Test
	@DisplayName("Sjekker om alle koordinatene blir returnert riktig")
	public void testGetAllCords() {
		Assertions.assertEquals(Arrays.asList(0,0,3,0),ship.getShipCords());
	}
	@Test
	@DisplayName("Sjekker om konstrukt�ren reagerer p� diagoanlt skip")
	public void testDiagonalShip() {
		try {
			Ship s = new Ship(0,0,1,1);
			fail("Ship is diagonal on the board");
		}
		catch(IllegalArgumentException e) {
			
		}
	}
	@Test 
	@DisplayName("Sjekkker om konstrukt�ren reagerer p� en len = 0")
	public void testZeroLength() {
		try {
			Ship s = new Ship(0,0,0,0);
			fail("Ship has a length of zero");
		}
		catch(IllegalArgumentException e) {
			
		}
	}
	

}
