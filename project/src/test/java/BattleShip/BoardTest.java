package BattleShip;

import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;

import battleShip.Board;
import battleShip.Ship;

public class BoardTest {
	Board boardPlayer;
	Board boardEnemy;
	Ship ship;
	Ship ship2;
	
	@BeforeEach
	public void setUp() {
		boardPlayer = new Board(5,true);
		boardEnemy = new Board(5,false);
		ship = new Ship(0,0,3,0);
		ship2 = new Ship(3,0,3,2);
	}
	
	@Test
	@DisplayName("Sjekker addship utl�ser et IllegalArgumentException n�r skip krysser")
	public void testCrossingShips() {
		boardPlayer.addAvShip(ship.getLen());
		boardPlayer.addAvShip(ship2.getLen());
		boardPlayer.addShip(ship);
		try {
			boardPlayer.addShip(ship2);
			fail("Ships crossed");
		}
		catch(IllegalArgumentException e) {
			
		}
	}
	@Test
	@DisplayName("Sjekker om addship utl�ser en IllegalArgumentException n�r lengden ikke er tilgjengelig")
	public void checkIfAvailable() {
		try {
			boardPlayer.addShip(ship);
			fail("Ship not avaiable");
		}
		catch(IllegalArgumentException e) {
			
		}
		
	}
	@Test
	@DisplayName("Sjekker om setBoard funker b�de med gyldig og ugyldig argument")
	public void checkSetBoard() {
		boardEnemy.setBoard(boardPlayer);
		//Setter brettene like n�r de har samme st�rresle
		Assertions.assertEquals(boardPlayer.getBoard(),boardEnemy.getBoard());
		//pr�ver � endre boardet til en ny st�rrelse.
		boardEnemy = new Board(7,false);
		try {
			boardEnemy.setBoard(boardPlayer);
		}
		catch (IllegalArgumentException e) {
			
		}
	}
	@Test
	@DisplayName("Sjekker gettere for brettet er rikitg")
	public void testShipNum() {
		boardPlayer.addAvShip(ship.getLen());
		boardPlayer.addShip(ship);
		//Antall skips-deler er likt lengden p� det eneste skipet
		Assertions.assertEquals(boardPlayer.getShipNum(), ship.getLen());
		//Verdien av 0,0 er ikke null, som betyr at et skip er der
		Assertions.assertTrue(boardPlayer.getValueCord(0, 0)!=null);
		//returnerer en liste med alle koordinatene til Ship-constructoren. Alts� er den lik en n�stet liste med cords til "ship"
		Assertions.assertEquals(boardPlayer.getEveryShipCords(), Arrays.asList(Arrays.asList(0,0,3,0)));
	}
	
	@Test
	@DisplayName("Sjekker at � legge til hits og misses funker, samt at gettere for listene funker")
	public void checkHitsAndMisses() {
		boardPlayer.addHit("23");
		boardPlayer.addMiss("33");
		//Begge koordinatene ligger i hver sin liste
		Assertions.assertTrue(boardPlayer.getHits().contains("23"));
		Assertions.assertTrue(boardPlayer.getMiss().contains("33"));
		//Skal kaste siden cords er out of range
		try {
			boardPlayer.addHit("77");
		}
		catch (IllegalArgumentException e) {
			
		}
		
	}
	@Test
	@DisplayName("Sjekker gydlig og ugyldig input til addAvShip og avShips")
	public void testAvShip() {
		boardPlayer.addAvShip(ship.getLen());
		//Lengden ligger i avShips
		Assertions.assertTrue(boardPlayer.getAvShips().contains(ship.getLen()));
		//lengde kan ikke v�re 0
		try {
			boardPlayer.addAvShip(0);
		}
		catch(IllegalArgumentException e) {
			
		}
		//St�rre enn brettet
		try {
			boardPlayer.addAvShip(7);
		}
		catch (IllegalArgumentException e){
			
		}
	}
	

}
