package BattleShip;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import battleShip.Board;
import battleShip.SaveHandler;
import battleShip.SaveInterface;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

public class SaveHandlerTest {
	SaveHandler s;
	SaveInterface l;
	Board contents;
	Board empty;
	//Alt av input til "saven" er feilh�dtert s� alt annet en navnet kan manipuleres av brukeren
	@BeforeEach
	public void setUp() {
		s = new SaveHandler();
		contents = new Board(5,false);
		empty = new Board(5,true);
	}
	@Test
	@DisplayName("Tester saveLocation")
	public void testSaveLocation() throws FileNotFoundException {
		//tester hvor ting blir lagret
		Assertions.assertEquals(s.getSavedFileLocation("save"),l.saveFolder+"save.txt" );
	}
	
	@Test
	@DisplayName("Tester saveGame og h�ndtering av navn")
	public void testSaveGame() throws IOException {
		//Begge brettene m� v�re initailisert, atls� ha like mange skip p� brettet, for at en kan lagre,
		//Kaster fordi empty ikke har noen skip
		try {
			s.saveGame(empty, contents, "test");
		}
		catch(IllegalArgumentException e) {
			
		}
		s.saveGame(contents, contents, "test");
		//tester at "test" er i savednames
		Assertions.assertTrue(s.getSavedNames().contains("test"));
		s.loadGame("test");
		Assertions.assertEquals(s.getSavedEnemyBoard().getEveryShipCords(), contents.getEveryShipCords());
		Assertions.assertEquals(s.getSavedPlayerBoard().getEveryShipCords(), contents.getEveryShipCords());
		//Pr�ver � lagre med samme navn
		try {
			s.saveGame(contents, contents, "test");
		}
		catch(IllegalArgumentException e) {
			
		}
		
	}

	
	
	


}
