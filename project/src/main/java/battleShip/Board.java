package battleShip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javafx.scene.Node;

public class Board {
	
	private int size;
	private ArrayList<ArrayList<Ship>> Board; // Todimensjoanlt array som er backenden av brettet
	private ArrayList<String> hits = new ArrayList<String>(); //Liste med koordinater med hits
	private ArrayList<String> miss = new ArrayList<String>(); //Samme liste med misses
	private ArrayList<Integer> AvailableShips = new ArrayList<Integer>(); // liste med skip en kan legge ut på brettet
	
	//Konstruerer arrayListen, og fyller det med skip if(!player)
	public Board(int size,boolean player) {
		this.size = size;
		ArrayList<ArrayList<Ship>> Board = new ArrayList<ArrayList<Ship>>(size);
		for(int i = 0; i < size; i++) {
			ArrayList<Ship> row = new ArrayList<Ship>();
			Board.add(row);
			for(int k = 0; k < size; k++) {
				row.add(null);
			}
		}
		this.Board = Board;
		if(!player) {
			generateEnemyBoard(size);
		}
	}
	//Legger til et ship på this.board
	//Valideret input for å sjekke om skipet ikke kryser med et annet, ikke går skrått eller har en ugyldig lengde
	//(en ugyldig lengde er 0 eller at den ikke er i "availableShips")
	public void addShip(Ship s) {
		ArrayList<Integer> l = s.getShipCords();
		int x1 = l.get(0);
		int y1 = l.get(1);
		int x2 = l.get(2);
		int y2 = l.get(3);
		int high;
		int low;
		//sjekker om lengden er mulig å legge ut på brettet
		if(this.getAvShips().contains(s.getLen())) {
			//Sjekker hvilken vei skipet blir plassert, enten er x like eller så er y like
			if(x1==x2) {
				high = Math.max(y1, y2);
				low = Math.min(y1, y2);
				//Sjekker først om alle rutene som er valgt ikke har noen andre skip innenfor intervallet
				for(int i = low; i <= high; i++) {
					if(this.Board.get(i).get(x1)!=null) {
						throw new IllegalArgumentException("Ships cannot cross");
					}
					
				}
				//Hvis intervallet er "klart", setter en inn skipet i arraylisten.
				for(int i = low; i <= high; i++) {
					this.Board.get(i).set(x1, s);	
				}
			}
			
			else {
				high = Math.max(x1, x2);
				low = Math.min(x1, x2);
				for(int i = low; i <= high; i++) {
					if(this.Board.get(y1).get(i)!=null) {
						throw new IllegalArgumentException("Ships cannot cross");
					}
				
				}
				for(int i = low; i <= high; i++) {
					this.Board.get(y1).set(i, s);
				}
			}
			
			
			//Hvis lengden, aka skipet, er i listen blir den fjernet fra listen etter den blir lagt på "BattleBoard"
			AvailableShips.remove(AvailableShips.indexOf(s.getLen()));
		}
		else {
			throw new IllegalArgumentException("Ship size is not available");
		}
	}
	
	public int getSize() {
		return this.size;
	}
	//Å prøve å lage en AI som oprettet egne skip virket litt for komplekst
	//Istednefor velger metoden et tilfeldig, satt brett for hver størrelse
	public void generateEnemyBoard(int size) {
		Random r = new Random();
		int rand = r.nextInt(3);
		if(size == 5) {
			this.AvailableShips.add(2);
			this.AvailableShips.add(2);
			this.AvailableShips.add(3);
			this.AvailableShips.add(4);
			
			if (rand == 0) {
				this.addShip(new Ship(0,0,1,0));
				this.addShip(new Ship(0,4,3,4));
				this.addShip(new Ship(2,2,3,2));
				this.addShip(new Ship(4,0,4,2));
			}
			else if(rand == 1) {
				this.addShip(new Ship(3,0,4,0));
				this.addShip(new Ship(0,2,0,1));
				this.addShip(new Ship(1,4,1,1));
				this.addShip(new Ship(4,4,4,2));
			}
			else {
				this.addShip(new Ship(0,0,0,2));
				this.addShip(new Ship(1,1,4,1));
				this.addShip(new Ship(1,3,2,3));
				this.addShip(new Ship(3,4,4,4));
			}
		}
		else if(size == 7) {
			this.addAvShip(2);
            this.addAvShip(3);
            this.addAvShip(3);
            this.addAvShip(4);
            this.addAvShip(6);
            
            if(rand == 0) {
            	this.addShip(new Ship(4,0,6,0));
				this.addShip(new Ship(1,4,1,1));
				this.addShip(new Ship(3,6,3,1));
				this.addShip(new Ship(4,2,4,4));
				this.addShip(new Ship(0,6,1,6));
            	
            }
            else if(rand == 1) {
            	this.addShip(new Ship(2,1,5,1));
				this.addShip(new Ship(0,3,0,2));
				this.addShip(new Ship(1,3,6,3));
				this.addShip(new Ship(1,5,3,5));
				this.addShip(new Ship(4,6,6,6));
            	
            }
            else {
            	this.addShip(new Ship(3,0,3,5));
				this.addShip(new Ship(5,0,6,0));
				this.addShip(new Ship(0,1,0,4));
				this.addShip(new Ship(5,4,5,2));
				this.addShip(new Ship(2,6,4,6));
            	
            }
			
		}
		else {
			this.addAvShip(2);
            this.addAvShip(2);
            this.addAvShip(3);
            this.addAvShip(4);
            this.addAvShip(5);
            this.addAvShip(6);
            this.addAvShip(7);
            
            if(rand == 0) {
            	this.addShip(new Ship(2,0,3,0));
				this.addShip(new Ship(1,1,1,7));
				this.addShip(new Ship(3,1,6,1));
				this.addShip(new Ship(8,1,8,2));
				this.addShip(new Ship(3,3,7,3));
            	this.addShip(new Ship(4,5,6,5));
				this.addShip(new Ship(3,7,8,7));
            }
            else if (rand == 1) {
				this.addShip(new Ship(2,0,3,0));
				this.addShip(new Ship(1,1,1,7));
				this.addShip(new Ship(3,1,6,1));
				this.addShip(new Ship(8,1,8,2));
				this.addShip(new Ship(3,3,7,3));
		    	this.addShip(new Ship(4,5,6,5));
				this.addShip(new Ship(3,7,8,7));
            	
            }
            else {
				this.addShip(new Ship(1,0,7,0));
				this.addShip(new Ship(3,6,3,1));
				this.addShip(new Ship(6,2,7,2));
				this.addShip(new Ship(5,6,5,3));
				this.addShip(new Ship(7,3,7,5));
		    	this.addShip(new Ship(7,7,8,7));
				this.addShip(new Ship(2,8,6,8));
            	
            }
			
		}
		
	}
	
	//Getters og setters for alle verdier en har bruk for
	public void setBoard(Board b) {
		if(this.getBoard().size() == b.getBoard().size()) {
			this.Board = b.getBoard();	
		}
		else {
			throw new IllegalArgumentException("Boards are not the same size");
		}
	}
	public int getShipNum() {
		int count = 0;
		for(ArrayList<Ship> a:this.getBoard()) {
			for(Ship s:a) {
				if(String.valueOf(s)!="null") {
					count+=1;
				}
				
			}
		}
		return count;
	}
	public Ship getValueCord(int x, int y) {
		return this.Board.get(y).get(x);
	}

	public ArrayList<ArrayList<Ship>> getBoard(){
		return this.Board;
	}
	public void addHit(String s) {
		String[] l = s.split("");
		if(Integer.valueOf(l[0])>size-1 || Integer.valueOf(l[1])>size-1) {
			throw new IllegalArgumentException("cords out of range");
		}
		if(this.getMiss().contains(s)) {
			throw new IllegalArgumentException("cords cant be both in hits and misses");
		}
		else {
			this.hits.add(s);	
		}
	}
	
	public ArrayList<String> getHits(){
		return this.hits;
	}
	
	public void addMiss(String s) {
		String[] l = s.split("");
		if(Integer.valueOf(l[0])>size-1 || Integer.valueOf(l[1])>size-1) {
			throw new IllegalArgumentException("cords out of range");
		}
		else {
			this.miss.add(s);	
		}
	}
	public ArrayList<String> getMiss(){
		return this.miss;
	}
	public void addAvShip(int s) {
		if(s>this.size) {
			throw new IllegalArgumentException("Length is bigger than size of board");
		}
		else if (s<=0){
			throw new IllegalArgumentException("Length cant be 0 or less than 0");
		}
		else {
			AvailableShips.add(s);
		}
	}
	public ArrayList<Integer> getAvShips(){
		return this.AvailableShips;
	}
	public ArrayList<ArrayList<Integer>> getEveryShipCords(){
		ArrayList<ArrayList<Integer>> l = new ArrayList<ArrayList<Integer>>();
		for(ArrayList<Ship> a : this.getBoard()) {
			for(Ship s : a) {
				if (s != null) {
					if(!l.contains(s.getShipCords())) {
						l.add(s.getShipCords());		
					}
				}
			}
		}
		return l;
	}
	


	public static void main(String[] args) {

}
}
