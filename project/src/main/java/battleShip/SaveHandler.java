package battleShip;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class SaveHandler implements SaveInterface {

	
	private void save(String s) throws IOException{
		try (FileWriter writer =new FileWriter(this.getSavedFileLocation("save"), true)) {
			PrintWriter w = new PrintWriter(writer);
			w.println(s);
			
		}
	}
	public void saveGame(Board player, Board enemy, String name) throws IOException {

		if(name == "") {
			throw new IllegalArgumentException("Must have characters");
		}
		else if(this.getSavedNames().contains(name)) {
			throw new IllegalArgumentException("Name is already in use");
		}
		else if(player.getShipNum() != enemy.getShipNum()) {
			throw new IllegalArgumentException("Uneven amount of ships, cant save");
		}
		
		String game = name;
		game += ";";
		game+=player.getSize();
		game += ";";
		
		for(String s:player.getHits()) {
			game+=s+" ";
		}
		
		game+=";";
		
		for(String s:player.getMiss()) {
			game+=s+" ";
		}
		
		game += ";";
		
		for(ArrayList<Integer> a : player.getEveryShipCords()) {
			for(int s:a) {
				game+=String.valueOf(s) + ":";
			}
			game+="-";
		}
		
		game+=";";
		
		for(String s:enemy.getHits()) {
			game+=s+"-";
		}
		
		game+=";";
		
		for(String s:enemy.getMiss()) {
			game+=s+"-";
		}
		
		game+=";";
		
		for(ArrayList<Integer> a : enemy.getEveryShipCords()) {
			for(int s:a) {
				game+=String.valueOf(s) + ":";
			}
			game+="-";
		}
		
		
		this.save(game);
		
	}

	public ArrayList<String> getSavedNames() throws FileNotFoundException{
		ArrayList<String> l = new ArrayList<String>();
		try(Scanner scanner =new Scanner(new File(this.getSavedFileLocation("save")))){
			while (scanner.hasNextLine()) {
				String[] s = scanner.nextLine().split(";");
				l.add(s[0]);
			}
		}
		return l;
	}
	
	public String getSavedFileLocation(String name) {
		return saveFolder + name +".txt";
	}
	private int size;
	Board player;
	Board enemy;
	
	public void loadGame(String name) throws FileNotFoundException {
		boolean found = false;
		try(Scanner scanner = new Scanner(new File(this.getSavedFileLocation("save")))){
			while (scanner.hasNextLine()) {
				
				String[] s = scanner.nextLine().split(";");
				if(s[0].equals(name)) {
					System.out.println(s);
					found = true;
					size = Integer.valueOf(s[1]);
					
					player = new Board(size, true);
					enemy = new Board(size,true);
					
					if(s[2]!="") {
						String[] l = s[2].split(" ");
						for(String k : l) {
							player.addHit(k);
						}
					}
					if(s[3]!="") {
						String[] l = s[3].split(" ");
						for(String k : l) {
							player.addMiss(k);
						}
					}
					
					String[] playerCords = s[4].split("-");
					for(String i : playerCords) {
						String[] c = i.split(":");
						int x1 = Integer.valueOf(c[0]);
						int y1 = Integer.valueOf(c[1]);
						int x2 = Integer.valueOf(c[2]);
						int y2 = Integer.valueOf(c[3]);
						player.addAvShip((Math.abs(y2-y1+x2-x1)+1));
						player.addShip(new Ship(x1,y1,x2,y2));
						
					}
					
					
					
					if(s[5]!="") {
						String[] l = s[5].split("-");
						for(String k : l) {
							enemy.addHit(k);
							System.out.println("Hits----" + k);
						}
					}
					if(s[6]!="") {
						String[] l = s[6].split("-");
						for(String k : l) {
							enemy.addMiss(k);
							System.out.println("Misses---"+k);
						}
					}
					String[] enemyCords = s[7].split("-");
					for(String i : enemyCords) {
						String[] c = i.split(":");
						int x1 = Integer.valueOf(c[0]);
						int y1 = Integer.valueOf(c[1]);
						int x2 = Integer.valueOf(c[2]);
						int y2 = Integer.valueOf(c[3]);
						enemy.addAvShip((Math.abs(y2-y1+x2-x1)+1));
						enemy.addShip(new Ship(x1,y1,x2,y2));
						
					}
				}
			}
		}
		if(!found) {
			throw new IllegalArgumentException("Name is not saved");
		}
	}
	
	public Board getSavedPlayerBoard() {
		return player;
	}
	public Board getSavedEnemyBoard() {
		return enemy;
	}
	


}
