package battleShip;



import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;



public class Controller {
	
	
	// Deklarerer stage som global slik at den kan lukkes etter en metode er igangsatt
	Stage settingScreen;
	Stage gameStage = new Stage();
	
	
	//Konfigurasjonen av "initScreen" der man legger skipene p� gridet
	Stage initStage = null;
	Board boardPlayer;
	Board boardEnemy;
	ArrayList<Integer> xy1 = null;
	ArrayList<Integer> xy2 = null;
	Button InitGridButton;
	GridPane initGrid;
	Label shipsLeft;
	
	private SaveInterface saveHandler = new SaveHandler();
	
	
	//Konfigurasjonen av "gameScreen" der spillet foreg�r
	int shipNum = 0; //Antall skipsdeler en kan treffe, brukes til counter
	Button saveGame = new Button("Save Game");
	Button loadGame = new Button("Load Game");
	
	Button hitGrid;
	
	GridPane gameGridPlayer = new GridPane();
	GridPane gameGridEnemy = new GridPane();
	HBox gamePane;
	Scene gameScene;
	String remaining = "Remaining ship-parts: ";
	
	//Boolean som viser hvor langt man er i prosessen
	boolean initializedPlayerStarted = false;
	boolean gameOver = false;
	
	//Viser "SettingScreen" der man velger hvilke vanskelighetsgrad man vil ha
		//Vnaskelighetsgraden bestemmes av hvor stort brettet er og hvor mange skip brettet inneholder
		public void showSettings() {
			gameOver = false;
			initializedPlayerStarted = false;
			Stage stage = new Stage();
			GridPane grid = new GridPane();
			Label easy = new Label("Available shiplengths are: 2,2,3 and 4");
			Label medium = new Label("Available shiplengths are: 2,3,3,4 and 6");
			Label hard = new Label("Available shiplengths are: 2,2,3,4,5,6 and 7");
			
			Button e = new Button("Easy");
			e.setOnMousePressed(event -> {
				stage.close();
				this.shipNum = 11;
				boardPlayer = new Board(5,true);
				boardEnemy = new Board(5,false);
	            boardPlayer.addAvShip(2);
	            boardPlayer.addAvShip(2);
	            boardPlayer.addAvShip(3);
	            boardPlayer.addAvShip(4);
				initShip(boardPlayer);
	        });
			
			Button m = new Button("Medium");
			m.setOnMousePressed(event -> {
				stage.close();
				boardPlayer = new Board(7,true);
				boardEnemy = new Board(7,false);
	            this.shipNum = 18;
	            boardPlayer.addAvShip(2);
	            boardPlayer.addAvShip(3);
	            boardPlayer.addAvShip(3);
	            boardPlayer.addAvShip(4);
	            boardPlayer.addAvShip(6);
	   			initShip(boardPlayer);
	        });
			
			Button h = new Button("Hard");
			h.setOnMousePressed(event -> {
				stage.close();
				boardPlayer = new Board(9,true);
				boardEnemy = new Board(9,false);
	            this.shipNum = 29;
	            boardPlayer.addAvShip(2);
	            boardPlayer.addAvShip(2);
	            boardPlayer.addAvShip(3);
	            boardPlayer.addAvShip(4);
	            boardPlayer.addAvShip(5);
	            boardPlayer.addAvShip(6);
	            boardPlayer.addAvShip(7);
	   			initShip(boardPlayer);
	        });
			e.setPrefSize(100, 50);
			m.setPrefSize(100, 50);
			h.setPrefSize(100, 50);
			e.setPadding(new Insets(10));
			h.setPadding(new Insets(10));
			m.setPadding(new Insets(10));
			grid.setHgap(20);
			grid.setVgap(20);
			grid.add(e, 0, 0);
			grid.add(easy, 1, 0);
			grid.add(m, 0, 1);
			grid.add(medium, 1, 1);
			grid.add(h, 0, 2);
			grid.add(hard, 1, 2);
			
			Scene s = new Scene(grid,500,500);
			stage.setScene(s);
			stage.show();
			settingScreen = stage;
		}
		
		//Tegner gridet etter hver gang man initialiserer skip.
	    public void initShip(Board s) { 
	    	initGrid = new GridPane();
	    	String sl = "Shiplengths left: ";
	    	for(int i:boardPlayer.getAvShips()) {
	    		sl+=String.valueOf(i) + "   ";
	    	}
	    	shipsLeft = new Label(sl);
	    	int max = s.getSize();
	    	int x = 0;
	    	int y = 0;
	    	for(ArrayList<Ship> a : s.getBoard()) {
				for (Ship b : a) {
					if (b == null) {
						InitGridButton = new Button(" ");
							InitGridButton.setOnMousePressed(e -> {
					            drawShip(e);
					        });
						}
					else {
						InitGridButton = new Button("O");
					}
					InitGridButton.setPrefSize(50, 50);
					if(x<max && y<max) {
						initGrid.add(InitGridButton, x, y);
					}
					x+=1;
				}
				x=0;
				y+=1;
			}
	    	if(!initializedPlayerStarted) {
	    		initStage = new Stage();
	    		shipsLeft.setPadding(new Insets(40));
				HBox box = new HBox(initGrid,shipsLeft);
				Scene initScene = new Scene(box, 800, 600);
				initStage.setScene(initScene);
				initStage.show();
				initializedPlayerStarted = true;
				
				Stage info = new Stage();
	    		Label l = new Label("Press a button on the grid once to start drawing your ship");
	    		Label l2 = new Label("press another button to complete a ship");
	    		Button ok = new Button("Ok");
	    		ok.setOnMousePressed(e -> {
		            info.close();
		        });
	    		VBox b = new VBox(l,l2,ok);
	    		Scene scene = new Scene(b);
	    		info.setScene(scene);
	    		info.show();
	    	}
	    	else {
	    		HBox box = new HBox(initGrid,shipsLeft);
	    		shipsLeft.setPadding(new Insets(40));
				Scene initScene = new Scene(box, 800, 600);
				initStage.setScene(initScene);	
	    	}
	    }
	    
	  //Metode som h�ndterer tegning a skip p� "initScreen" tar inn koordinater ogs� velger turnswitchen om det er xy1 eller xy2
		//Pr�ver � legge til Ship, om det ikke g�r vil en alert-screen oprettes som informerer om erroren og lar brukeren pr�ve igjen
	    @FXML
	    boolean turnswitch = true;
	    private void drawShip(MouseEvent e) {
	        Node source = (Node)e.getSource();
	        Integer c = GridPane.getColumnIndex(source);
	        Integer r = GridPane.getRowIndex(source);
	        ArrayList<Integer> l = new ArrayList<Integer>();
	        l.add(c.intValue());
	        l.add(r.intValue());
	        if(turnswitch) {
	        	xy1 = l;
	        	turnswitch=false;
	        }
	        else {
	        	xy2 = l;
	        	try {
	        		boardPlayer.addShip(new Ship(xy1.get(0),xy1.get(1),xy2.get(0),xy2.get(1)));
	        	}
	        	catch(Exception error) {
	        		String s = String.valueOf(error);
	        		Alert alert = new Alert(AlertType.INFORMATION);
	        		alert.setTitle("Invalid action");
	        		alert.setHeaderText(s);
	        		alert.showAndWait();
	        	}
	        	initShip(boardPlayer);
	        	turnswitch = true;
	        }
	        if(boardPlayer.getAvShips().size() == 0) {
	        	initStage.close();
	        	Game(true);
	        	gameStage = new Stage();
	        	gameStage.setScene(gameScene);
				gameStage.show();
				
				Stage info = new Stage();
	    		Label l1 = new Label("Press a button on the enemy grid to start playing");
	    		Label l2 = new Label("The enemy will guess after you turn. Good Luck!");
	    		Button ok = new Button("Ok");
	    		ok.setOnMousePressed(event -> {
		            info.close();
		        });
	    		VBox b = new VBox(l1,l2,ok);
	    		Scene scene = new Scene(b);
	    		info.setScene(scene);
	    		info.show();
	        }
	        
	    }
	   
	    
	  //Metode som tegner "GameScreen", g�r gjennom spiller og fienden sitt brett og sjekker med "Hits" om hvilke skip som er truffet
		// pTurn avgjlr om det er spillerens tur eller ikke, if(pTurn) avgj�r om gameGridEnemy er clickable med metoden "hitGrid(e)"
		public void Game(boolean pTurn) {
			gameGridPlayer = new GridPane();
			gameGridEnemy = new GridPane();
			for(int i = 0; i < boardPlayer.getSize(); i++) {
				for (int k = 0; k < boardPlayer.getSize(); k++) {

					String cords = ""+k+i;
					//Sjekker om cords er "hit","miss" eller "ship p� spillersiden og tegner gridet 
					if(boardEnemy.getHits().contains(cords)) {
						hitGrid = new Button("X");
					}
					else if (boardEnemy.getMiss().contains(cords)) {
						hitGrid = new Button("M");
					}
					else if(boardPlayer.getValueCord(i, k) == null) {
						hitGrid = new Button(" ");
					}
					else {
						hitGrid = new Button("O");
					}
					hitGrid.setPrefSize(50, 50);
					gameGridPlayer.add(hitGrid,i,k);
					
					//Gj�r det samme p� fiendeSiden bare en kun viser "hit" og "miss"
					if(boardPlayer.getHits().contains(cords)) {
						hitGrid = new Button("X");
					}
					else if (boardPlayer.getMiss().contains(cords)) {
						hitGrid = new Button("M");
					}
					else {
						hitGrid =  new Button(" ");
						if(pTurn) {
							hitGrid.setOnMousePressed(e -> {
					            hitGrid(e);
					        });
						}
					}
					hitGrid.setPrefSize(50, 50);
					gameGridEnemy.add(hitGrid, i, k);
						}
					}
			
				//Sjekker om antall treff er like mange som antall mulige skip p� brettet, gj�res p� hver side etter hvert fors�k p� treff
				if(shipNum == boardEnemy.getHits().size()||shipNum == boardPlayer.getHits().size()) {
					gameStage.close();
					gameOver = true;
				}
				if(gameOver) {
					gameOver();
				}
				Button save = new Button("Save Game");
				Button load = new Button("Load Game");
				save.setOnMousePressed(e -> {
		            saveGame();
		        });
				load.setOnMousePressed(e -> {
					Stage confirm = new Stage();
					Label text = new Label("This action will end your game, and you will lose all unsaved progress");
					Label text2 = new Label("Continue to load game?");
					Button yes = new Button("Yes");
					Button no = new Button("No");
					yes.setOnMousePressed(event -> {
						try {
							showSaved();
							gameStage.close();
							confirm.close();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
			        });
					no.setOnMousePressed(event -> {
			            confirm.close();
			        });
					HBox buttons = new HBox(yes,no);
					VBox v = new VBox(text,text2,buttons);
					Scene s = new Scene(v);
					confirm.setScene(s);
					confirm.show();
		        });
				Label filler = new Label("");
				
				filler.setPrefHeight(100);
				
				Label remPlayer = new Label(remaining + (boardPlayer.getShipNum()-boardEnemy.getHits().size()));
				Label remEnemy = new Label(remaining + (boardPlayer.getShipNum()-boardPlayer.getHits().size()));
				
				VBox buttons = new VBox(save,filler,load);
				VBox pBox = new VBox(gameGridPlayer,remPlayer);
				VBox eBox = new VBox(gameGridEnemy,remEnemy);
				HBox grids = new HBox(pBox,eBox);
				
				pBox.setPadding(new Insets(40));
				eBox.setPadding(new Insets(40));
				grids.setPrefWidth(900);
				gamePane = new HBox(grids,buttons);
				gameScene = new Scene(gamePane,1200,600);
				gameStage.setScene(gameScene);
			}
		
		
		//Viser lagrede spill
		
		public void showSaved() throws IOException {
			Stage stage = new Stage();
			GridPane savedGrid = new GridPane();
			
			
			int count = 0;
			for(String str : saveHandler.getSavedNames()) {
				Button b = new Button("Play       Name of Game: " + str);
				b.setPrefSize(300, 50);
				b.setLineSpacing(10);
				b.setOnMousePressed(e -> {
		            try {
		            	stage.close();
						saveHandler.loadGame(str);
						boardPlayer = saveHandler.getSavedPlayerBoard();
						boardEnemy = saveHandler.getSavedEnemyBoard();
						shipNum = boardPlayer.getShipNum();
						gameStage = new Stage();
						Game(true);
			        	gameStage.setScene(gameScene);
						gameStage.show();
						gameOver = false;
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}
		        });
				savedGrid.add(b, 0, count);
				count += 1;
			}
			Scene scene =  new Scene(savedGrid,600,600);
			stage.setScene(scene);
			stage.show();
		}
		
		public void saveGame() {
			Stage stage = new Stage();
			final TextField name = new TextField();
			Button b = new Button("Save Game");
			b.setOnMousePressed(e -> {
	            SaveHandler s = new SaveHandler();
	            try {
					s.saveGame(boardPlayer,boardEnemy,name.getText());
					stage.close();
				} catch (Exception e1) {
					String error = String.valueOf(e1);
	        		Alert alert = new Alert(AlertType.INFORMATION);
	        		alert.setTitle("Invalid action");
	        		alert.setHeaderText(error);
	        		alert.showAndWait();
				}
	        });
			GridPane container = new GridPane();
			container.add(name,0,0);
			container.add(b, 0, 1);
			
			Scene scene = new Scene(container);
			stage.setScene(scene);
			stage.show();
			
		}

	private void gameOver() {
		Stage over = new Stage();
		Label one = new Label("Game Over");
		Label two = null;
		Label three = null;
		Label four = null;
		boolean playerWin = boardPlayer.getHits().size() > boardEnemy.getHits().size();
		if(playerWin) {
			two = new Label("You won!");
			three = new Label("You hit " + boardPlayer.getHits().size() + " times");
			four = new Label("You missed " + boardPlayer.getMiss().size() + " times");
		}
		else {
			two = new Label("The Enemy Won!");
			three = new Label("The enemy hit " + boardEnemy.getHits().size() + " times");
			four = new Label("The enemy missed " + boardPlayer.getMiss().size() + " times");
		}
		one.setPadding(new Insets(50));
		two.setPadding(new Insets(50));
		three.setPadding(new Insets(50));
		four.setPadding(new Insets(50));
		VBox box = new VBox(one,two,three,four);
		Scene c = new Scene(box,600,600);
		over.setScene(c);
		over.show();
	}
	
	//H�ndterer skudd fra spillerens side
	private void hitGrid(MouseEvent e) {
		Node source = (Node)e.getSource();
	  	Integer c = GridPane.getColumnIndex(source);
	    Integer r = GridPane.getRowIndex(source);
	    if(boardEnemy.getValueCord(r,c)!=null) {
	    	boardPlayer.addHit(""+r+c);
	    }
	    else {
	    	boardPlayer.addMiss(""+r+c);
	    }
			 Game(false);
			 if(!gameOver) {
				 enemyHit();
				 Game(true);	 
			 }
	    }
	
	private void enemyHit() {
		Random r = new Random();
		int n = r.nextInt(boardPlayer.getSize()*boardPlayer.getSize());
		for(int i = 0; i < boardPlayer.getSize(); i++) {
			for(int k = 0; k < boardPlayer.getSize(); k++) {
				String cords = ""+k+i;
				if(n == 0 && !boardEnemy.getMiss().contains(cords) && !boardEnemy.getHits().contains(cords)) {
					if(boardPlayer.getValueCord(i, k) == null) {
						boardEnemy.addMiss(cords);
					}
					else {
						boardEnemy.addHit(cords);
					}
				}
				else if (n == 0) {
					enemyHit();
				}
				n-=1;
			}
		}
	}
		

	
	

}
