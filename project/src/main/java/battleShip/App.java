package battleShip;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class App extends Application{
	
	@Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("ui.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("BattleShip");
        stage.setScene(scene);
        stage.show();
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

}
