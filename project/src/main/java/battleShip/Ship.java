package battleShip;

import java.util.ArrayList;

public class Ship {
	
	private int x1;
	private int y1;
	private int x2;
	private int y2;
	private int len;

	
	public Ship(int x1,int y1,int x2, int y2) {
		//Sjekker om skipet ikke går skrått og at lengden ikke er 0
		if ((x2-x1 == 0) ^ (y2-y1==0)) {
			this.len = Math.abs(y2-y1+x2-x1)+1;
		}
		else if((x2-x1==0)&&(y2-y1==0)) {
			throw new IllegalArgumentException("Cant have a lenght of 0");
		}
		else {
			throw new IllegalArgumentException("Ship cannot be diagonal");
		}
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
	
	public ArrayList<Integer> getShipCords() {
		ArrayList<Integer> s = new ArrayList<Integer>();
		s.add(this.x1);
		s.add(this.y1);
		s.add(this.x2);
		s.add(this.y2);
		return s;
	}
	public int getLen() {
		return this.len;
	}
	public int getX1() {
		return this.x1;
	}
	public int getY1() {
		return this.y1;
	}
	public int getX2() {
		return this.x2;
	}
	public int getY2() {
		return this.y2;
	}
	
	
	

	
	


	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
