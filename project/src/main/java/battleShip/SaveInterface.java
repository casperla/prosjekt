package battleShip;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public interface SaveInterface {

	String saveFolder = "src/main/resources/saves/";
	public void saveGame(Board player,Board enemy,String name) throws IOException;
	public void loadGame(String name) throws FileNotFoundException;
	public ArrayList<String> getSavedNames() throws FileNotFoundException;
	public Board getSavedPlayerBoard();
	public Board getSavedEnemyBoard();
}
